﻿using System;
using System.Collections.Generic;
using System.Text;

using SourceCode.Workflow.Client;

namespace K2UpdateTesting
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press Enter to continue");
            Console.ReadLine();
            RunK2Stuff();
        }

        static void RunK2Stuff()
        {
            SourceCode.Workflow.Client.Connection myConn = new Connection();
            SourceCode.Workflow.Client.ProcessInstance myProc;
            //SourceCode.Workflow.Client.Action myAction;

            myConn.Open("brbnek2");

            //myProc = myConn.OpenProcessInstance(3223);
            //myProc.DataFields["ECO_ChangeOrder_Short"].Value = "16329";

            myProc = myConn.OpenProcessInstance(68947);
            myProc.DataFields["ADGroupDescription"].Value = "k2: BOM";
            myProc.DataFields["ADGroupName"].Value = "BRENCL-AU\\k2_ECC_BOM";

            //myProc.Update();
            myConn.Close();
        }
    }
}
